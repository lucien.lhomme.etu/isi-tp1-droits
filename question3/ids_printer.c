#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {
    FILE *f;
    char s;
    int euid = geteuid();
    int egid = getegid();
    int ruid = getuid();
    int rgid = getgid();
    printf("%d\n", euid);
    printf("%d\n", egid);
    printf("%d\n", ruid);
    printf("%d\n", rgid);
    f = fopen("mydir/mydata.txt", "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    while((s=fgetc(f))!=EOF) {
        printf("%c",s);
    }
    printf("File opens correctly\n");
    fclose(f);
    exit(EXIT_SUCCESS);
}