# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- LAURENT, Louis, email: louis.laurent.etu@univ-lille.fr

- LHOMME, Lucien, email: lucien.lhomme.etu@univ-lille.fr

## Question 1

Réponse

Le processus peut écrire car toto fait parti du groupe ubuntu. Seul ce groupe a le droit d'écrire dans le fichier, toto (s'il ne faisait pas parti du groupe) ainsi que les autres utilisateurs peuvent seulement le lire.

## Question 2

Le caractère x pour un répertoire signifie le droit d'accès à ce répertoire. En enlevant le droit d'accès au groupe ubuntu, l'utilisateur toto n'a pas le droit d'accèder à ce répertoire parce que ce dernier fait parti du groupe ubuntu. 
En essayant de lister le contenu du répertoire depuis l'utilisateur toto, la seule donnée auquel nous ayons accès est le type de fichier ainsi que son nom. Toutes les autres informations sont masquées par des points d'interrogation.

## Question 3

Les valeurs des différents id sont toutes à 1001. Le processus ne parvient pas à ouvrir le fichier mydata.txt.
Après activation du flag set-user-id, les valeurs des id sont encore de 1001 sauf l'EUID qui cette fois est de 1000. Le fichier est maintenant accessible par l'utilisateur toto.

## Question 4

Les deux ids ont comme valeurs 1001.

## Question 5

La commande chfn permet de changer le nom du vrai utilisateur et les informations.

Le résultat de "ls -al /usr/bin/chfn" est : -rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
Tout utilisateur à le droit d'exécuter la commande chfn pour modifier leurs propres informations et le root acquiert le droit de modifier les informations de n'importe quel utilisateur.

Les informations ont bien été mise à jours, l'utilisateur à bien le droit de modifier ses propres informations.

## Question 6

Autre part, sinon n'importe qui aurait le droit de lire le mots de passe de tout le monde.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

![img](question8/Screenshot-from-2022-01-19-17-17-00.png)

## Question 9

Le programme et les scripts dans le repertoire *question9*.

- pwg with crypt :

![img](question9/Capture_pwg.PNG)

- rmg with crypt :

![img](question9/Capture_rmg.PNG)

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 

- Côté Serveur :

![img](question10/Capture_Server.png)

- Côté Client :

![img](question10/Capture_Client.png)







