#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include "check_pass.h"
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char* argv[]) {

    struct stat buf;

    if (argc == 2) {
        stat(argv[1], &buf);

        if (buf.st_gid == getgid()) {
            printf("Entrez votre mot de passe : ");
            char pass[64];
            fgets(pass, 64, stdin);
            if (check_pass(getuid(), pass) == 0) {
                remove(argv[1]);
                printf("Le fichier à bien été supprimé.\n");
            } else {
                printf("T'as pas le bon mot de passe.\n");
            }
        } else {
            printf("T'as pas le bon groupe.\n");
        }
    }

}