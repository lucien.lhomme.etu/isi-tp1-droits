#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int check_pass(__uid_t user, char *pass) {

    char * line = NULL;
    size_t len = 0;
    size_t read;

    FILE *fp = fopen("/home/admin/passwd", "r");
    if (fp == NULL) {
        printf("fopen failed, errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }

    while ((read = getline(&line, &len, fp)) != -1) {
        unsigned long i = 0;
        while (i < len && line[i] != ':') {
            i++;
        }
        char vOut[64];
        snprintf(vOut, 64, "%d", user);
        if (strncmp(vOut, line, i) == 0) {
            if (strncmp(pass, &line[i+1], len-1-i) == 0) {
                return 0;
            }
        }
    }

    fclose(fp);

    return 1;
}