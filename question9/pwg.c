#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <crypt.h>

#define _XOPEN_SOURCE
#define SALT "42"

int main(int argc, char* argv[]) {

    char * line = NULL;
    size_t len = 0;
    size_t read;

    FILE *fp = fopen("passwd", "r+");
    if (fp == NULL) {
        printf("fopen failed, errno = %d\n", errno);
        exit(EXIT_FAILURE);
    }
    //int idxToWrite = 0;
    int new = 0;
    while ((read = getline(&line, &len, fp)) != -1) {
        unsigned long i = 0;
        while (i < len && line[i] != ':') {
            i++;
        }
        char vOut[64];
        snprintf(vOut, 64, "%d", getuid());
        if (strncmp(vOut, line, i) == 0) {
            printf("Entrez votre ancien mot de passe : ");
            char pass[64];
            fgets(pass, 64, stdin);
            pass[strcspn(pass, "\n")] = 0;
            if (strncmp(crypt(pass, SALT), &line[i+1], read-2-i) == 0) {
                new = 1;
                break;
            }
            printf("T'as pas le bon mot de passe.\n");
            return 1;
        }
    }

    fpos_t pos1;
    fgetpos(fp, &pos1);
    if (new == 1) {
        pos1.__pos -= read;
        fsetpos(fp, &pos1);
        printf("Entrez votre nouveau mot de passe : ");
    } else {
        printf("Entrez votre mot de passe : ");
    }
    char pass[64];
    fgets(pass, 64, stdin);
    pass[strcspn(pass, "\n")] = 0;
    char vOut[130];
    snprintf(vOut, 129, "%d%c%s\n", getuid(), ':', crypt(pass, SALT));
    vOut[129] = 0;

    fprintf(fp, "%s", vOut);

    fclose(fp);

    return 0;
}