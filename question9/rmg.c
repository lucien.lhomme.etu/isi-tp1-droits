#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include "check_pass.h"
#include <unistd.h>
#include <sys/types.h>
#include <crypt.h>

#define _XOPEN_SOURCE
#define SALT "42"

int main(int argc, char* argv[]) {

    struct stat buf;

    if (argc == 2) {
        stat(argv[1], &buf);

        if (buf.st_gid == getgid()) {
            printf("Entrez votre mot de passe : ");
            char pass[64];
            fgets(pass, 64, stdin);
            pass[strcspn(pass, "\n")] = 0;
            if (check_pass(getuid(), crypt(pass, SALT)) == 0) {
                remove(argv[1]);
                printf("Le fichier à bien été supprimé.\n");
            } else {
                printf("T'as pas le bon mot de passe.\n");
            }
        } else {
            printf("T'as pas le bon groupe.\n");
        }
    }

}