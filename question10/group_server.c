#include <sys/types.h>
#include <arpa/inet.h>	
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "check_pass.h"

typedef struct infos {
    char *pass;
    uid_t uid;
    gid_t gid;
} Infos;

void parseInfos(struct infos *infos, char *buff, int size) {
    int idxSpace = 0;
    while (idxSpace < size && buff[idxSpace] != ' ') idxSpace++;

    infos->pass = malloc(idxSpace+1);
    memcpy(infos->pass, buff, idxSpace);
    infos->pass[idxSpace] = 0;

    int idxSpace2 = idxSpace+1;
    while (idxSpace2 < size && buff[idxSpace2] != ' ') idxSpace2++;
    
    char uid[idxSpace2-idxSpace+1];
    memcpy(uid, &buff[idxSpace+1], idxSpace2-idxSpace);
    uid[idxSpace2-idxSpace] = 0;
    infos->uid = atoi(uid);

    char gid[size-idxSpace2+1];
    memcpy(gid, &buff[idxSpace2+1], size-idxSpace2);
    gid[size-idxSpace2] = 0;
    infos->gid = atoi(gid);
}

int main(int argc, char **argv) {

    int socket_serveur = socket(PF_INET, SOCK_STREAM, 0);
    if (socket_serveur == -1) {
        perror("socket_serveur");
        exit(1);
    }

    //Création de l'interface réseau
    struct sockaddr_in saddr;
    saddr.sin_family = AF_INET; /*IPV4 */
    saddr.sin_port = htons(4000); /* Port d'écoute */
    saddr.sin_addr.s_addr = INADDR_ANY; /* écoute sur toutes les interfaces */

    //Setsockopt
    int optval = 1;
    if (setsockopt(socket_serveur, SOL_SOCKET, SO_REUSEADDR, & optval, sizeof(int)))
        perror("Can not set SO_REUSEADDR option");

    //Binding
    if (bind(socket_serveur, (struct sockaddr *)&saddr, sizeof(saddr)) == -1) {
        perror("bind socket_serveur");
        exit(1);
    } else {
        printf("bind socket_serveur is very good\n");
    }

    //Ecoute
    if (listen(socket_serveur, 10) == -1) {
        perror("listen socket_serveur");
        exit(1);
    } else {
        printf("listen socket_serveur is very good\n");
    }

    int run = 1;
    while (run) {
        printf("accept...\n");
        fflush(stdout);
        int socket_client = accept(socket_serveur, NULL, NULL);
        if (socket_client == -1) {
            if (errno == EINTR)
                continue;
            perror("accept socket client");
            exit(1);
        }

        int pid = fork();
        if (pid == 0) {
            int MAX_BUFF = 128;
            char buff[MAX_BUFF];
            int size = recv(socket_client, buff, MAX_BUFF, 0);

            Infos *infos = malloc(sizeof(Infos));
            parseInfos(infos, buff, size);

            printf("pass = %s\n", infos->pass);
            printf("uid = %d\n", infos->uid);
            printf("gid = %d\n", infos->gid);
            fflush(stdout);

            if (check_pass(infos->uid, infos->pass) != 0) {
                printf("T'as pas le bon mot de passe.\n");
                write(socket_client, "failed", 7);
                fflush(stdout);
                return 1;
            }

            seteuid(infos->uid);
            setegid(infos->gid);

            write(socket_client, "logged", 7);

            while ((size = recv(socket_client, buff, MAX_BUFF, 0)) != 0) {
                if (strncmp(buff, "close", 5) == 0) {
                    write(socket_client, "close", 6);
                    close(socket_client);
                    exit(0);
                } else if (strncmp(buff, "list ", 5) == 0) {
                    int gpid = fork();
                    if(gpid == 0){ // spawn a grandchild to exec one command
                        seteuid(infos->uid);
                        setegid(infos->gid);

                        close(socket_serveur);
                        dup2(socket_client, STDOUT_FILENO);  /* duplicate socket on stdout */
                        dup2(socket_client, STDERR_FILENO); /* duplicate stderr on stdout */
                        
                        char dir[size-4];
                        memcpy(dir, &buff[5], size-4);
                        dir[size-5] = 0;

                        int status_code = execl("/bin/ls", "ls", dir, (char *)0);
                        if (status_code == -1) {
                            printf("Syntax error in argument\n");
                            fflush(stdout);
                        }
                    }
                } else if (strncmp(buff, "read ", 5) == 0) {
                    int gpid = fork();
                    if(gpid == 0){ // spawn a grandchild to exec one command
                        seteuid(infos->uid);
                        setegid(infos->gid);

                        close(socket_serveur);
                        dup2(socket_client, STDOUT_FILENO);  /* duplicate socket on stdout */
                        dup2(socket_client, STDERR_FILENO); /* duplicate stderr on stdout */
                        
                        char file[size-4];
                        memcpy(file, &buff[5], size-4);
                        file[size-5] = 0;

                        int status_code = execl("/bin/cat", "cat", file, (char *)0);
                        if (status_code == -1) {
                            printf("Syntax error in argument\n");
                            fflush(stdout);
                        }
                    }
                }
            }
        } else if (pid == -1) {
            perror("fork");
            close(socket_client);
            exit(1);
        } else {
            // Mon fils l'a copié, moi je le ferme sinon j'ai trop de fichiers ouverts.
            close(socket_client);
        }
    }
        
    return 0;

}
