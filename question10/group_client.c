#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include "check_pass.h"
#include <crypt.h>

#define SALT "42"

int main(int argc, char **argv) {

    if(argc!=2) {
        printf("Need 1 argument, the command file\n");
        exit(0);
    }

    char * line = NULL;
    size_t len = 0;
    size_t read;

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        exit(EXIT_FAILURE);
    }
    read = getline(&line, &len, fp);
    int idxSpace = 0;
    while (idxSpace < read && line[idxSpace] != ' ') {
        idxSpace++;
    }
    char pass[read-idxSpace-1];
    memcpy(pass, &line[idxSpace+1], read-idxSpace-2);
    pass[strcspn(pass, "\n")] = 0;
    if (check_pass(getuid(), crypt(pass, SALT)) != 0) {
        printf("T'as pas le bon mot de passe.\n");
        fflush(stdout);
        return 1;
    }

    struct sockaddr_in serv;
    int socket_serv = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_serv == -1) {
        printf("socket creation failed...\n");
        exit(0);
    } else {
        printf("Socket successfully created..\n");
    }
    // IP and port address
    serv.sin_family = AF_INET;
    serv.sin_port = htons(4000);
    serv.sin_addr.s_addr = inet_addr("127.0.0.1");

    // Connect to server
    if (connect(socket_serv, (struct sockaddr*)&serv, sizeof(serv)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    } else {
        printf("connected to the server..\n");
        int MAX = 128;
        char infos[MAX];
        int n = sprintf(infos, "%s %d %d", crypt(pass, SALT), getuid(), getgid());
        write(socket_serv, infos, n);
        
        char buff[MAX];
        recv(socket_serv, buff, MAX, 0);
        printf("%s\n\n", buff);
        if (strncmp("logged", buff, 6) != 0) {
            printf("loging failed\n");
            return 1;
        }

        while ((read = getline(&line, &len, fp)) != -1) {
            //printf("read = %ld\n", read);
            line[read-1] = 0;
            printf("%s :\n", line);
            write(socket_serv, line, read);
            
            long size = recv(socket_serv, buff, MAX, 0);
            //printf("buff = %s\n", buff);
            //printf("recv_size = %ld\n", size);
            buff[size] = 0;
            printf("%s\n", buff);
        }
    }

    fclose(fp);

    return 0;

}