# peuvent lire tous les fichiers et sous-répertoires contenus dans dir_a et dir_c ;
chmod g+r dir_a dir_c
# peuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer, ni créer des nouveaux fichiers
chmod g-w dir_c
# peuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_a, et peuvent créer de nouveaux fichiers et répertoires dans dir_a ;
chmod g+w dir_a
# n’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_a qui ne leur appartiennent pas ;
chmod +t dir_a
# ne peuvent pas ni lire, ni modifier, ni effacer les fichiers dans dir_b, et ne peuvent pas créer des nouveaux fichiers dans dir_b.
chmod o-rw dir_b


# drwxrwx--t 2 admin groupe_a 4096 Jan 1 00:00 dir_a